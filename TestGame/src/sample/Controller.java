package sample;

import javafx.animation.AnimationTimer;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.image.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.*;

public class Controller {



    /**
     * Total score count
     */
    int scoreCount = 0;
    public int timeInterval = 5000;

    /**
     *  Application path
     */
    String basePath = "";

    /**
     * Image view dependency
     */
    @FXML private ImageView imageView;

    @FXML
    private Text scoreValue;


    @FXML protected void handleSubmitButtonAction(ActionEvent event){
        System.out.println("Test Event");
    }

    /**
     * Take food from plate
     * @param event
     */
    @FXML protected void eatFood(MouseEvent event){

        try {
            final long startTime =  System.currentTimeMillis();
            ImageView foodPlate = (ImageView) event.getSource();
            System.out.println(foodPlate.getId()+ "Here");
            //calculate score here
            this.scoreCount = this.scoreCount + this.getClickedFoodScore(foodPlate.getId(),"heartScore");
            scoreValue.setText(Integer.toString(scoreCount));
            System.out.println(this.scoreCount+" SCORE");
            // update to new food in use interface
            int randomIndex = this.getRandomIndex();
            Food randFood = (Food)this.getFoods().get(randomIndex);
            foodPlate.setId(randFood.foodPicture);
            foodPlate.setImage(this.randomFood(randFood));

//            new AnimationTimer() {
//                @Override
//                public void handle(long timestamp) {
//                    long now = System.currentTimeMillis();
//                    long timeDifference = now - startTime;
//                    if(timeDifference >= timeInterval){
//
//                        System.out.println(timeDifference);
//                    }
//                }
//            }.start();

        }
        catch(Exception e){
            System.out.println(e.getMessage() + "Exception" + e.getCause());
        }

    }

    /**
     * Get random image object from food array list
     * @param randFood
     * @return Image
     */
    public Image randomFood(Food randFood){
        URL foodUrl = this.getClass().getResource("");
        Image food = new Image(foodUrl.toString()+ "foodPictures/"+randFood.foodPicture);
        return food;
    }



    /**
     * index for food array list
     * @return random index
     */
    public int getRandomIndex(){
        int randomIndex = (int)(Math.random() * this.getFoods().size());
        return randomIndex;
    }

    /**
     * Prepare food array
     * @return Food Array List
     */
    public ArrayList getFoods(){

        ArrayList<Food> foodlist = new ArrayList();
        //apple.png
        Food apple = new Food("Apple","apple.png",200,200);
        foodlist.add(apple);

        //beef_bangla.png
        Food beefBangla = new Food("Beef Bangla","beef_bangla.png",200,200);
        foodlist.add(beefBangla);

        //beef_biryani.png
        Food beefBiryani = new Food("Beef Biryani","beef_biryani.png",200,200);
        foodlist.add(beefBiryani);

        //beef_burger.png
        Food beefBurger = new Food("Beef Burger","beef_burger.png",200,200);
        foodlist.add(beefBurger);

        //beef_steak.png
        Food beefSteak = new Food("Beef Steak","beef_steak.png",200,200);
        foodlist.add(beefSteak);

        //carrot.png
        Food carrot = new Food("Carrot","carrot.png",200,200);
        foodlist.add(carrot);

        //chicken_biryani.png
        Food chickenBiryani = new Food("Chicken  Biryani","chicken_biryani.png",200,200);
        foodlist.add(chickenBiryani);

        // chicken_fry.png
        Food chickenFry = new Food("Chicken Fry","chicken_fry.png",200,200);
        foodlist.add(chickenFry);

        // chicken_roast.png
        Food chickenRoast = new Food("Chicken Roast","chicken_roast.png",200,200);
        foodlist.add(chickenRoast);

        // chicken_sausage.png
        Food chickenSausage = new Food("Chicken Sausage","chicken_sausage.png",200,200);
        foodlist.add(chickenSausage);

        // coca_cola.png
        Food cocaCola = new Food("Coca Cola","coca_cola.png",200,200);
        foodlist.add(cocaCola);

        // cucumber.png
        Food cucumber = new Food("Cucumber","cucumber.png",200,200);
        foodlist.add(cucumber);

        // curd.png
        Food curd = new Food("Curd","curd.png",200,200);
        foodlist.add(curd);

        // egg_boil.png
        Food eggBoil = new Food("Egg Boil","egg_boil.png",200,200);
        foodlist.add(eggBoil);

        // egg_omlet.png
        Food eggOmlet = new Food("Egg Omlet","egg_omlet.png",200,200);
        foodlist.add(eggOmlet);

        // empty_plate.png
        //Food emptyPlate = new Food("Empty Plate","empty_plate.png",200,200);
        //foodlist.add(emptyPlate);

        // fish.png
        Food fish = new Food("Fish","fish.png",200,200);
        foodlist.add(fish);

        // french_fry.png
        Food frenchFry = new Food("French Fry","french_fry.png",200,200);
        foodlist.add(frenchFry);

        // jilebi.png
        Food jilebi = new Food("Jilebi","jilebi.png",200,200);
        foodlist.add(jilebi);

        // mutton_biryani.png
        Food muttonBiryani = new Food("Mutton Biryani","mutton_biryani.png",200,200);
        foodlist.add(muttonBiryani);

        // mutton_roast.png
        Food muttonRoast = new Food("Mutton Roast","mutton_roast.png",200,200);
        foodlist.add(muttonRoast);

        // noodles.png
        Food noodles = new Food("Noodles","noodles.png",200,200);
        foodlist.add(noodles);

        // orange.png
        Food orange = new Food("Orange","orange.png",200,200);
        foodlist.add(orange);

        // papaya.png
        Food papaya = new Food("Papaya","papaya.png",200,200);
        foodlist.add(papaya);

        // pastry_cake.png
        Food pastryCake = new Food("Pastry cake","pastry_cake.png",200,200);
        foodlist.add(pastryCake);

        // pauruti.png
        Food pauruti = new Food("Pauruti","pauruti.png",200,200);
        foodlist.add(pauruti);

        // pomegranate.png
        Food pomegranate = new Food("Pomegranate","pomegranate.png",200,200);
        foodlist.add(pomegranate);

        // ruti.png
        Food ruti = new Food("Ruti","ruti.png",200,200);
        foodlist.add(ruti);

        // sandwitch.png
        Food sandwitch = new Food("Sandwitch","sandwitch.png",200,200);
        foodlist.add(sandwitch);

        // spinach.png
        Food spinach = new Food("Spinach","spinach.png",200,200);
        foodlist.add(spinach);

        // sweet.png
        Food sweet = new Food("Sweet","sweet.png",200,200);
        foodlist.add(sweet);

        // vegetable.png
        Food vegetable = new Food("Vegetable","vegetable.png",200,200);
        foodlist.add(vegetable);

        // water_melon.png
        Food waterMelon = new Food("Water Melon","water_melon.png",200,200);
        foodlist.add(waterMelon);

        return foodlist;
    }

    private int getClickedFoodScore(String foodPicture, String scoreType){

        int score = 0;
        Iterator itr = this.getFoods().iterator();
        while(itr.hasNext()){
            Food food=(Food) itr.next();
            if(food.foodPicture == foodPicture){
                if(scoreType == "heartScore"){
                    score = food.heartScore;
                }
                if(scoreType == "diabeticScore"){
                    score = food.diabeticScore;
                }
            }
        }
        return score;
    }


}
