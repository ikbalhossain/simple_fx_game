package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.*;
import javafx.stage.Stage;
import javafx.scene.image.ImageView;
import javafx.animation.*;
import javafx.event.*;
import javafx.util.*;
import java.util.Random;

public class Main extends Application {

    public int totalScore = 0;
    public int timeInterval = 5000;
    public long startTime;


    @Override
    public void start(Stage primaryStage) throws Exception{
        this.startTime = System.currentTimeMillis();
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Dain Table");
        //Group root2 = new Group(root);
        final ImageView imgviewArray[] = new ImageView[9];
        final long intervalArray[] = new long[9];

        for (int i = 0; i<9;i++){
            String plateId = "#empty_plate_0"+(i+1);
            ImageView foodPlate = (ImageView) root.lookup(plateId);
            this.getRandomFood(foodPlate);
            imgviewArray[i] = foodPlate;
        }

          Random rand = new Random();
          int randomNum = rand.nextInt((9 - 0) + 1) + 0;

//        for (int i = 0; i<9;i++){
            final int imageIndex = randomNum;
            new AnimationTimer() {
                @Override
                public void handle(long timestamp) {
                    intervalArray[imageIndex] = startTime;
                    long now = System.currentTimeMillis();
                    long timeDifference = now - intervalArray[imageIndex];
                    if(timeDifference >= timeInterval){
                        ImageView foodPlate = imgviewArray[imageIndex];
                        intervalArray[imageIndex] = now;
                        System.out.println(foodPlate.toString());
                    }
                }
            }.start();
//        }


        Scene primaryScene = new Scene(root, 846, 634);
        primaryScene.getStylesheets().addAll(this.getClass().getResource("style.css").toExternalForm());
        primaryStage.setScene(primaryScene);
        primaryStage.setResizable(false);
        primaryStage.show();
    }


    public static void main(String[] args) {
        try {
            launch(args);
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }


    private void getRandomFood(ImageView images){
        // update to new food in use interface
        Controller con = new Controller();
        int randomIndex = con.getRandomIndex();
        Food randFood = (Food)con.getFoods().get(randomIndex);
        images.setId(randFood.foodPicture);
        images.setImage(con.randomFood(randFood));
    }
}
