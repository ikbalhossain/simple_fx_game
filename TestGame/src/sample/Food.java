package sample;

public class Food {
    int heartScore;
    int diabeticScore;
    String name;
    String foodPicture;

    public Food(String name, String foodPicture, int heartScore, int diabeticScore){
        this.name = name;
        this.foodPicture = foodPicture;
        this.heartScore = heartScore;
        this.diabeticScore = diabeticScore;
    }

}
